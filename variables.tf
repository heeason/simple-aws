variable "aws_instance_ami" {
  type = "string"
  description = "Generated"
  default = "ami-085925f297f89fce1"
}

variable "aws_instance_aws_instance_type" {
  type = "string"
  description = "Generated"
  default = "t2.micro"
}

variable "availability_zone" {
  type = "string"
  description = "Generated"
  default = "us-east-1a"
}

variable "region" {
    type = "string"
    description = "Generated"
    default = "us-east-1"
}

variable "key_name" {
    type = "string"
    description = "Generated"
    default = ""
}

variable "access_key" {
    type = "string"
    description = "Generated"
    default = ""
}

variable "secret_key" {
    type = "string"
    description = "Generated"
    default = ""
}