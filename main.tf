terraform {
  required_version = "> 0.8.0"
}

provider "aws" {
  version = "~> 1.8"
  region     = "${var.region}"
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
}


resource "aws_instance" "aws_instance" {
  ami = "${var.aws_instance_ami}"
  instance_type = "${var.aws_instance_aws_instance_type}"
  availability_zone = "${var.availability_zone}"
  key_name = "${var.key_name}"
}